import React from "react"
import ProfessionList from "./ProfessionList"

export default class ProfessionalField extends React.Component{
		constructor(props){
				super(props)
				this.state = {stateSelect: "close", professional: ""}
				this.filterProf = this.filterProf.bind(this)
				this.setFieldValue = this.setFieldValue.bind(this)

		}

		setFieldValue(value){
				this.setState({professional: value})
		}

		filterProf(){
				return ProfessionList.filter(item => (
						item.value.toLowerCase().indexOf(this.state.professional.toLowerCase())) === 0)
		}

		render() {
				return(
						<div>
							<input
								className="professionfield-input"
								required
								autoComplete="off"
								type="text"
								name="professional"
								value={this.state.professional}
								onChange={event => {
										this.setFieldValue(event.target.value)
								}}
								onBlur={() => {
										this.setState({stateSelect: "close"})
								}}
								onFocus={() => {
										this.setState({stateSelect: "open"})
								}}
							/>
								{
										(this.state.stateSelect === "open") && (this.filterProf().length > 0)
										&& (this.state.professional.length > 0) &&
												<ul className={"drop-list select-" + this.state.stateSelect} >
														{this.filterProf().map(item =>
															<li key={item.id}
																	className="drop-list-item"
																	onMouseDown={() => {
																			this.setFieldValue(item.value)
																	}}>
																	<strong>{item.value.substring(0, this.state.professional.length)}</strong>
																	{item.value.substring(this.state.professional.length)}
															</li>
														)}
												</ul>
								}
						</div>
				)
		}

}