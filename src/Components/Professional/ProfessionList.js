const ProfList = [
    {
        id: 1,
        value: "Слесарь"
    },
    {
        id: 2,
        value: "Слесарь-сборщик"
    },
    {
        id: 3,
        value: "Сантехник"
    },
    {
        id: 4,
        value: "Дизайнер"
    },
    {
        id: 5,
        value: "Дизайнер интерьеров"
    },
    {
        id: 6,
        value: "Лоншафтный дизайнер"
    }
];

export default ProfList;