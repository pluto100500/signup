import React from "react"
import PhoneList from "./PhoneList"

export default class PhoneField extends React.Component{
		constructor(props){
				super(props)
				this.state = {stateSelect: "close", code: this.props.phoneCode,
						phone: "", flag: this.props.flag, country: this.props.country}
				this.phoneValidator = this.phoneValidator.bind(this)
		}

		phoneValidator(v){
				this.setState({phone: v.replace(/(\d{3})(\d{3})(\d{2})(\d{2})/, "$1 $2-$3-$4")})
		}

		render(){
				return(
						<div className="phonefield">
							<div className="phonefield-row">
								<div className="phonefield-country-wrap">
										<input
											className={"phonefield-country flag flag-" + this.state.flag}
											readOnly
											type="text"
											onClick={() => {
													this.setState({stateSelect: (this.state.stateSelect === 'close') ? 'open' : 'close'})
											}}
											onBlur={() => {
													this.setState({stateSelect: "close"})
											}}
										/>

								</div>
								<div
										className="phonefield-input-wrap"
										>
										<span className="phonefield-code">{this.state.code}</span>
										<div>
												<input
													required
													autoComplete={"off"}
													ref="phoneFieldInput"
													name="phone"
													className="phonefield-input"
													placeholder="495 123-45-67"
													value={this.state.phone}
													maxLength={10}
													onChange={(event) => this.phoneValidator(event.target.value)}
												/>
										</div>
								</div>
							</div>
							<ul className={"phonefield-country-list drop-list select-" + this.state.stateSelect}>
									{PhoneList.map(item =>
										<div
											key={item.id}
											className={"phonefield-country-list-item drop-list-item"}
											onMouseDown={event =>
													this.setState({code: item.code, flag: item.ind, country: item.country})
											}
										>
												<div className={"div-flag flag flag-" + item.ind}>{item.country}</div>

										</div>
									)}
							</ul>
						</div>
				)
		}
}