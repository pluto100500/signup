const CountryList = [
    {
        id: 1,
        country: "Russia",
        code: "+7",
        ind: "ru",
        img: "https://emojipedia-us.s3.amazonaws.com/thumbs/160/twitter/103/flag-for-russia_1f1f7-1f1fa.png"
    },
    {
        id: 2,
        country: "Japan",
        code: "+81",
        ind: "jp",
        img: "https://emojipedia-us.s3.amazonaws.com/thumbs/160/twitter/103/flag-for-japan_1f1ef-1f1f5.png"
    },
    {
        id: 3,
        country: "France",
        code: "+33",
        ind: "fr",
        img: "https://emojipedia-us.s3.amazonaws.com/thumbs/160/twitter/103/flag-for-france_1f1eb-1f1f7.png"
    },
];

export default CountryList;