import React, { Component } from 'react';
import "./index.css"
import ProfessionalField from "./Components/Professional/Professional"
import PhoneField from "./Components/Phone/Phone"

class App extends Component {
  render() {
    return (
      <form
        className="signup"
        onSubmit={(e) => {
            e.preventDefault()
            alert("Вы зарегистрировалсь")
        }}
      >
          <div className="title align-center"><strong>Зарегистрируйтесь</strong> и начните продавать услуги через интернет сегодня</div>
          <div className="row">
              <div className="col">
                  <label>Имя</label>
                  <input
                      required
                      name="name"
                      type="text"/>
              </div>
              <div className="col">
                  <label>Фамилия</label>
                  <input
                  required
                  name="surname"
                  type="text"/>
              </div>
          </div>
          <div className="row">
              <div className="col">
                  <label>Профессия</label>
                  <ProfessionalField/>
              </div>
          </div>
          <div className="row">
              <div className="col">
                  <label>Телефон</label>
                  <PhoneField
                    phoneCode="+7"
                    flag="ru"
                    country="Russia"
                  />
              </div>
          </div>
          <div className='actions align-center'>
              <button
              type="submit"
              className="form-item">
                Зарегистрироваться
              </button>
          </div>
      </form>
    );
  }
}

export default App;
